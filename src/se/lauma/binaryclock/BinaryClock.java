/*
 * BinaryClock.java
 * 
 * First attempt at a binary clock for the android platform.
 * 
 * Four rows with six radio buttons each are used to show the binary values
 * The first two columns are the hour, next two are minutes and the last two
 * are seconds. 
 * 
 * A worker thread is used to sleep for one second and the post an event to the
 * UI thread that will update the clock. This is to ensure that the applications
 * is responsive. Not good to let the UI thread sleep
 * 
 */

package se.lauma.binaryclock;

import java.util.Calendar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

public class BinaryClock extends Activity implements Runnable {

    // About dialog ID
    public static final int DIALOG_ABOUT_ID = 1;

    // The worker thread object
    Thread runner;

    // Runnable used for posting events to the UI thread
    final Runnable updater = new Runnable() {
        public void run() {
            updateClock();
        };
    };

    // The handler used to post back update events from the worker thread
    // to the UI thread
    final Handler mHandler = new Handler();

    // The buttons used to show the binary values
    RadioButton hh8;
    RadioButton hl8;
    RadioButton mh8;
    RadioButton ml8;
    RadioButton sh8;
    RadioButton sl8;
    RadioButton hh4;
    RadioButton hl4;
    RadioButton mh4;
    RadioButton ml4;
    RadioButton sh4;
    RadioButton sl4;
    RadioButton hh2;
    RadioButton hl2;
    RadioButton mh2;
    RadioButton ml2;
    RadioButton sh2;
    RadioButton sl2;
    RadioButton hh1;
    RadioButton hl1;
    RadioButton mh1;
    RadioButton ml1;
    RadioButton sh1;
    RadioButton sl1;

    // Used to show the decimal values
    TextView textHH;
    TextView textHL;
    TextView textMH;
    TextView textML;
    TextView textSH;
    TextView textSL;

    private SharedPreferences preferences;

    // Create the options menu defined in the XML file main_menu.xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    // Someone selected an item on the options menu, handle it
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.about:
            about();
            return true;
        case R.id.prefs:
            Intent intent = new Intent(this, Preferences.class);
            startActivity(intent);
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    // Called when the activity is first created.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // Get all button objects
        hh8 = (RadioButton) findViewById(R.id.hh8);
        hl8 = (RadioButton) findViewById(R.id.hl8);
        mh8 = (RadioButton) findViewById(R.id.mh8);
        ml8 = (RadioButton) findViewById(R.id.ml8);
        sh8 = (RadioButton) findViewById(R.id.sh8);
        sl8 = (RadioButton) findViewById(R.id.sl8);
        hh4 = (RadioButton) findViewById(R.id.hh4);
        hl4 = (RadioButton) findViewById(R.id.hl4);
        mh4 = (RadioButton) findViewById(R.id.mh4);
        ml4 = (RadioButton) findViewById(R.id.ml4);
        sh4 = (RadioButton) findViewById(R.id.sh4);
        sl4 = (RadioButton) findViewById(R.id.sl4);
        hh2 = (RadioButton) findViewById(R.id.hh2);
        hl2 = (RadioButton) findViewById(R.id.hl2);
        mh2 = (RadioButton) findViewById(R.id.mh2);
        ml2 = (RadioButton) findViewById(R.id.ml2);
        sh2 = (RadioButton) findViewById(R.id.sh2);
        sl2 = (RadioButton) findViewById(R.id.sl2);
        hh1 = (RadioButton) findViewById(R.id.hh1);
        hl1 = (RadioButton) findViewById(R.id.hl1);
        mh1 = (RadioButton) findViewById(R.id.mh1);
        ml1 = (RadioButton) findViewById(R.id.ml1);
        sh1 = (RadioButton) findViewById(R.id.sh1);
        sl1 = (RadioButton) findViewById(R.id.sl1);

        // Get all text objects
        textHH = (TextView) findViewById(R.id.TextHH);
        textHL = (TextView) findViewById(R.id.TextHL);
        textMH = (TextView) findViewById(R.id.TextMH);
        textML = (TextView) findViewById(R.id.TextML);
        textSH = (TextView) findViewById(R.id.TextSH);
        textSL = (TextView) findViewById(R.id.TextSL);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        
        applyPreferences();
        
        updateClock(); // update the clock

        // Create a separate thread to do the one second tick
        if (runner == null) {
            runner = new Thread(this);
            runner.start();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        applyPreferences();
    }
    
    private void applyPreferences() {
        boolean showDec = preferences.getBoolean("showDecimal", true);
        int visible = showDec ? View.VISIBLE : View.INVISIBLE;
        textHH.setVisibility(visible);
        textHL.setVisibility(visible);
        textMH.setVisibility(visible);
        textML.setVisibility(visible);
        textSH.setVisibility(visible);
        textSL.setVisibility(visible);
    }

    // A dialog should be created
    @Override
    protected Dialog onCreateDialog(int id, Bundle args) {
        Dialog dialog;

        switch (id) {
        case DIALOG_ABOUT_ID:
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.aboutMessage)
                    .setCancelable(true)
                    .setNeutralButton(R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int id) {
                                    dialog.dismiss();
                                }
                            });
            dialog = builder.create();
            break;
        default:
            dialog = null;
        }
        return dialog;
    }

    // Called when the activity is destroyed
    @Override
    public void onPause() {
        super.onPause();
    }

    // Convert decimal to binary
    boolean bin[][] = {
            // 8 4 2 1
            { false, false, false, false }, // 0
            { false, false, false, true }, // 1
            { false, false, true, false }, // 2
            { false, false, true, true }, // 3
            { false, true, false, false }, // 4
            { false, true, false, true }, // 5
            { false, true, true, false }, // 6
            { false, true, true, true }, // 7
            { true, false, false, false }, // 8
            { true, false, false, true } }; // 9

    // Update the clock, this is called approx. twice per second
    private void updateClock() {
        Calendar cal = Calendar.getInstance();

        int hours = cal.get(Calendar.HOUR_OF_DAY);
        int minutes = cal.get(Calendar.MINUTE);
        int seconds = cal.get(Calendar.SECOND);

        // Get the individual digits
        int hourTens = hours / 10;
        int hourOnes = hours % 10;
        int minuteTens = minutes / 10;
        int minuteOnes = minutes % 10;
        int secondTens = seconds / 10;
        int secondOnes = seconds % 10;

        // Update the decimal values
        textHH.setText(Integer.toString(hourTens));
        textHL.setText(Integer.toString(hourOnes));
        textMH.setText(Integer.toString(minuteTens));
        textML.setText(Integer.toString(minuteOnes));
        textSH.setText(Integer.toString(secondTens));
        textSL.setText(Integer.toString(secondOnes));

        // Update the binary values
        hh8.setChecked(bin[hourTens][0]);
        hh4.setChecked(bin[hourTens][1]);
        hh2.setChecked(bin[hourTens][2]);
        hh1.setChecked(bin[hourTens][3]);
        hl8.setChecked(bin[hourOnes][0]);
        hl4.setChecked(bin[hourOnes][1]);
        hl2.setChecked(bin[hourOnes][2]);
        hl1.setChecked(bin[hourOnes][3]);

        mh8.setChecked(bin[minuteTens][0]);
        mh4.setChecked(bin[minuteTens][1]);
        mh2.setChecked(bin[minuteTens][2]);
        mh1.setChecked(bin[minuteTens][3]);
        ml8.setChecked(bin[minuteOnes][0]);
        ml4.setChecked(bin[minuteOnes][1]);
        ml2.setChecked(bin[minuteOnes][2]);
        ml1.setChecked(bin[minuteOnes][3]);

        sh8.setChecked(bin[secondTens][0]);
        sh4.setChecked(bin[secondTens][1]);
        sh2.setChecked(bin[secondTens][2]);
        sh1.setChecked(bin[secondTens][3]);
        sl8.setChecked(bin[secondOnes][0]);
        sl4.setChecked(bin[secondOnes][1]);
        sl2.setChecked(bin[secondOnes][2]);
        sl1.setChecked(bin[secondOnes][3]);
    }

    //
    private void about() {
        showDialog(DIALOG_ABOUT_ID);
    }

    public void run() {
        while (runner != null) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
            mHandler.post(updater); // Post back to UI thread
        }
    }
}